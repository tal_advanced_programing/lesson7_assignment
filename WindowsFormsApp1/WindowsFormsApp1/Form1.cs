﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Globalization;
using System.Resources;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string[] _cards;
        private string _opponentCard;
        private string _yourCard;
        private NetworkStream clientStream;
        private string _lastCard;

        private int _yourScore;
        private int _opponentScore;

        public Form1()
        {
            InitializeComponent();
            _yourCard = "000";
            _opponentCard = "000";
            _cards = new string[] { "_2_of_clubs", "_2_of_diamonds", "_2_of_hearts", "_2_of_spades", "_3_of_clubs", "_3_of_diamonds", "_3_of_hearts", "_3_of_spades", "_4_of_clubs", "_4_of_diamonds", "_4_of_hearts", "_4_of_spades", "_5_of_clubs", "_5_of_diamonds", "_5_of_hearts", "_5_of_spades", "_6_of_clubs", "_6_of_diamonds", "_6_of_hearts", "_6_of_spades", "_7_of_clubs", "_7_of_diamonds", "_7_of_hearts", "_7_of_spades", "_8_of_clubs", "_8_of_diamonds", "_8_of_hearts", "_8_of_spades", "_9_of_clubs", "_9_of_diamonds", "_9_of_hearts", "_9_of_spades", "_10_of_clubs", "_10_of_diamonds", "_10_of_hearts", "_10_of_spades", "ace_of_clubs", "ace_of_diamonds", "ace_of_hearts", "ace_of_spades", "jack_of_clubs", "jack_of_diamonds", "jack_of_hearts", "jack_of_spades", "king_of_clubs", "king_of_diamonds", "king_of_hearts", "king_of_spades", "queen_of_clubs", "queen_of_diamonds", "queen_of_hearts", "queen_of_spades" };

            _lastCard = "";

            _yourScore = 0;
            _opponentScore = 0;

            Thread listening = new Thread(Listening);
            listening.Start();
        }

        private void GenerateCards()
        {
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(10 , 400);
            
            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::WindowsFormsApp1.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(84, 121);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    pictureBox1.Image = Properties.Resources.card_back_blue;


                    foreach (Control k in this.Controls)
                    {
                        if (k.Name.Equals(_lastCard))
                        {
                            ((PictureBox)k).Image = Properties.Resources.card_back_red;
                        }
                    }


                    Random rnd = new Random();
                    int card = rnd.Next(52);
                    
                    ((PictureBox)sender1).Image = (Image)Properties.Resources.ResourceManager.GetObject(_cards[card]);

                    _lastCard = ((PictureBox)sender1).Name;
                    Thread sendCard = new Thread(() => SendCard(card));
                    sendCard.Start();
                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10; 

            }
        }

        private void Freeze()
        {
            foreach (Control i in this.Controls)
            {
                i.Enabled = false;
            }
        }

        private void UnFreeze()
        {
            foreach (Control i in this.Controls)
            {
                i.Enabled = true;
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Your Score: " + _yourScore + "\nOpponent's Score: " + _opponentScore, "Game Results");
            Application.Exit();
        }

        private void Listening()
        {
            bool temp = false;

            do
            {
                try
                {
                    Invoke((MethodInvoker)delegate { GenerateCards(); });

                    Invoke((MethodInvoker)delegate { Freeze(); });


                    TcpClient client = new TcpClient();

                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

                    client.Connect(serverEndPoint);

                    clientStream = client.GetStream();    // open port
                    temp = true;
                }
                catch
                {

                }
            } while (temp == false);
            bool flag = true;
            while (flag)
            {
                try
                {
                    byte[] buffer = new byte[4];
                    int bytesRead = clientStream.Read(buffer, 0, 4);
                    string input = new ASCIIEncoding().GetString(buffer);       //message from the server

                    if (input.Equals("0000"))
                    {
                        Invoke((MethodInvoker)delegate { UnFreeze(); });
                    }
                    else if (input[0] == '2')
                    {
                        flag = false;
                        MessageBox.Show("Your Score: " + _yourScore + "\nOpponent's Score: " + _opponentScore, "Game Results");
                        Application.Exit();
                    }
                    else if (input[0] == '1')
                    {
                        _opponentCard = input.Substring(1);
                        if (_yourCard.Equals("000"))
                        {
                            Thread game = new Thread(PlayGame);
                            game.Start();
                        }
                    }
                }
                catch
                {

                }
            }

        }

        private void PlayGame()
        {
            string card = "";
            while((_opponentCard.Equals("000")) || (_yourCard.Equals("000")))
            {
                Thread.Sleep(200);
            }

            int MyCard = int.Parse(_yourCard.Substring(0, 2));
            int opponentCard = int.Parse(_opponentCard.Substring(0, 2));

            if (opponentCard >= 2 && opponentCard <= 10)
                card = '_' + (opponentCard).ToString() + '_' + "of" + '_';
            else if (opponentCard == 1)
                card = "ace_of_";
            else if (opponentCard == 11)
                card = "jack_of_";
            else if (opponentCard == 12)
                card = "queen_of_";
            else if (opponentCard == 13)
                card = "king_of_";

            if (_opponentCard[2] == 'C')
                card = card + "clubs";
            else if (_opponentCard[2] == 'D')
                card = card + "diamonds";
            else if (_opponentCard[2] == 'H')
                card = card + "hearts";
            else
                card = card + "spades";

            pictureBox1.Image = (Image)Properties.Resources.ResourceManager.GetObject(card);

            if(opponentCard == 1 && MyCard != 1)
            {
                _opponentScore++;

                Invoke((MethodInvoker)delegate { opponent_score.Text = _opponentScore.ToString(); });
            }
            else if(opponentCard != 1 && MyCard == 1)
            {
                _yourScore++;
                Invoke((MethodInvoker)delegate { your_score.Text = _yourScore.ToString(); });
            }
            else if(opponentCard > MyCard)
            {
                _opponentScore++;
               
                Invoke((MethodInvoker)delegate { opponent_score.Text = _opponentScore.ToString(); });
            }
            else if(opponentCard < MyCard)
            {
                _yourScore++;
                Invoke((MethodInvoker)delegate { your_score.Text = _yourScore.ToString(); });
            }


            _yourCard = "000";
            _opponentCard = "000";
        }


        private string ConvertCard(int card)
        {
            string cardCode = "";
            switch (card)
            {
                case 0:
                    cardCode = "02C";
                    break;

                case 1:
                    cardCode = "02D";
                    break;

                case 2:
                    cardCode = "02H";
                    break;

                case 3:
                    cardCode = "02S";
                    break;

                case 4:
                    cardCode = "03C";
                    break;

                case 5:
                    cardCode = "03D";
                    break;

                case 6:
                    cardCode = "03H";
                    break;

                case 7:
                    cardCode = "03S";
                    break;

                case 8:
                    cardCode = "04C";
                    break;

                case 9:
                    cardCode = "04D";
                    break;

                case 10:
                    cardCode = "04H";
                    break;

                case 11:
                    cardCode = "04S";
                    break;

                case 12:
                    cardCode = "05C";
                    break;

                case 13:
                    cardCode = "05D";
                    break;

                case 14:
                    cardCode = "05H";
                    break;

                case 15:
                    cardCode = "05S";
                    break;

                case 16:
                    cardCode = "06C";
                    break;

                case 17:
                    cardCode = "06D";
                    break;

                case 18:
                    cardCode = "06H";
                    break;

                case 19:
                    cardCode = "06S";
                    break;

                case 20:
                    cardCode = "07C";
                    break;

                case 21:
                    cardCode = "07D";
                    break;

                case 22:
                    cardCode = "07H";
                    break;

                case 23:
                    cardCode = "07S";
                    break;

                case 24:
                    cardCode = "08C";
                    break;

                case 25:
                    cardCode = "08D";
                    break;

                case 26:
                    cardCode = "08H";
                    break;

                case 27:
                    cardCode = "08S";
                    break;

                case 28:
                    cardCode = "09C";
                    break;

                case 29:
                    cardCode = "09D";
                    break;

                case 30:
                    cardCode = "09H";
                    break;

                case 31:
                    cardCode = "09S";
                    break;

                case 32:
                    cardCode = "10C";
                    break;

                case 33:
                    cardCode = "10D";
                    break;

                case 34:
                    cardCode = "10H";
                    break;

                case 35:
                    cardCode = "10S";
                    break;

                case 36:
                    cardCode = "01C";
                    break;

                case 37:
                    cardCode = "01D";
                    break;

                case 38:
                    cardCode = "01H";
                    break;

                case 39:
                    cardCode = "01S";
                    break;

                case 40:
                    cardCode = "11C";
                    break;

                case 41:
                    cardCode = "11D";
                    break;

                case 42:
                    cardCode = "11H";
                    break;

                case 43:
                    cardCode = "11S";
                    break;

                case 44:
                    cardCode = "13C";
                    break;

                case 45:
                    cardCode = "13D";
                    break;

                case 46:
                    cardCode = "13H";
                    break;

                case 47:
                    cardCode = "13S";
                    break;

                case 48:
                    cardCode = "12C";
                    break;

                case 49:
                    cardCode = "12D";
                    break;

                case 50:
                    cardCode = "12H";
                    break;

                case 51:
                    cardCode = "12S";
                    break;
            }

            return cardCode;
        }

        /*
         The function send the card that selected to the server
         */
        private void SendCard(int place)
        {
            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes('1' + ConvertCard(place));

                _yourCard = ConvertCard(place);

                clientStream.Write(buffer, 0, 4);
                clientStream.Flush();

                if (_opponentCard.Equals("000"))
                {
                    Thread play = new Thread(PlayGame);
                    play.Start();
                }
            }
            catch
            {

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show("Your Score: " + _yourScore + "\nOpponent's Score: " + _opponentScore, "Game Results");
        }
    }
}
